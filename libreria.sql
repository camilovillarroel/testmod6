/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : MySQL
 Source Server Version : 80013
 Source Host           : localhost:3306
 Source Schema         : libreria

 Target Server Type    : MySQL
 Target Server Version : 80013
 File Encoding         : 65001

 Date: 28/01/2019 13:42:20
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for categoria
-- ----------------------------
DROP TABLE IF EXISTS `categoria`;
CREATE TABLE `categoria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) DEFAULT NULL,
  `descripcion` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of categoria
-- ----------------------------
BEGIN;
INSERT INTO `categoria` VALUES (1, 'Aventuras', 'Los protas se pegan mansos viajes');
INSERT INTO `categoria` VALUES (2, 'Terror', 'Los mansos sustos que te pegai');
INSERT INTO `categoria` VALUES (3, 'Romance', 'Lamina teni cida y era un wn operao');
INSERT INTO `categoria` VALUES (4, 'Suspenso', 'En estos libros lo que pasa es que de pronto...');
COMMIT;

-- ----------------------------
-- Table structure for libro
-- ----------------------------
DROP TABLE IF EXISTS `libro`;
CREATE TABLE `libro` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) DEFAULT NULL,
  `autor` varchar(50) DEFAULT NULL,
  `editorial` varchar(59) DEFAULT NULL,
  `ejemplares` int(11) DEFAULT NULL,
  `categoria_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `categoria_id` (`categoria_id`),
  CONSTRAINT `libro_ibfk_1` FOREIGN KEY (`categoria_id`) REFERENCES `categoria` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of libro
-- ----------------------------
BEGIN;
INSERT INTO `libro` VALUES (2, 'Harry Potter', 'J.K.Rowling', 'Salamandra', 8, 1);
INSERT INTO `libro` VALUES (13, 'Papelucho', 'Marcela Paz', 'ZigZag', 12, 1);
INSERT INTO `libro` VALUES (14, 'El Resplandor', 'Stephen King', 'DeBolsillo', 7, 2);
INSERT INTO `libro` VALUES (15, 'Romeo y Julieta', 'William Shakespeare', 'ZigZag', 5, 3);
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
