package main;

import controllers.MainViewController;
import views.MainView;

public class Principal {
    public static void main(String[] args) {
        new MainViewController(new MainView()).inicio();
    }
    
}
