package controllers;

import views.MainView;
import views.LibroView;
import views.HomeView;
import models.LibroModel;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import models.CatModel;
import views.CatView;

/**
 * Esta clase controla las acciones que realiza la ventana principal.
 * 
 * @author luisherrera
 */
public class MainViewController extends Controller{
    private MainView vista = new MainView();

    public MainViewController(MainView vista){
        this.vista = vista;
    }

    /**
     * inicia los parámetros del JFrame.
     */
    public void iniciaVistaPrincipal(){
        vista.setTitle("LIBRERIA");
        vista.setLocationRelativeTo(null);
        vista.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        vista.setVisible(true);
        

    }
    
    /**
     * Asigna los listeners a los componentes de la vista (Menu).
     */
    public void asignaListeners(){
        vista.jmMantLibro.addActionListener(this);
        vista.jmHome.addMouseListener(this);
        vista.jmMantCat.addActionListener(this);

    }
    
    /**
     * Segundo método ejecutado luego del constructor. Esto arranca la
     * vista principal.
     */
    public void inicio(){
        iniciaVistaPrincipal();
        asignaListeners();
        cargaElHome();

    }
    
    public void cargaElPanelLibros(){
        LibroView libroView = new LibroView();
        
        // se inicia el controlador de personas.
        LibroController libroController = new LibroController(
                libroView, 
                new LibroModel()
        );
        libroController.iniciar();
        renovarPanel(libroView);
        
    }
    public void cargaElPanelCategorias(){
        CatView catView = new CatView();
        
        // se inicia el controlador de categorias.
        CatController catController = new CatController(
                catView, 
                new CatModel()
        );
        catController.iniciar();
        renovarPanel(catView);
        
    }
    
    public void cargaElHome(){
        renovarPanel(new HomeView());
    }
    

    private void renovarPanel(JPanel panel){
        vista.jpPadre.removeAll();
        vista.jpPadre.add(panel);
        vista.revalidate();
        vista.repaint();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        // menú de mant personas
        if(e.getSource() == vista.jmMantLibro) cargaElPanelLibros();
        if(e.getSource() == vista.jmMantCat) cargaElPanelCategorias();
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if(e.getSource() == vista.jmHome) cargaElHome();
    }
    
}
