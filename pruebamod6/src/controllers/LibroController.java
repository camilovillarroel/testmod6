package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import javax.swing.JTable;
import models.CatModel;
import models.LibroModel;
import views.LibroView;
import views.utils.Alerta;


public class LibroController extends Controller{
    LibroView vista;
    LibroModel modelo;


    public LibroController(LibroView vista, LibroModel modelo) {
        this.vista = vista;
        this.modelo = modelo;

    }
    

    public void iniciar(){
        vista.jbMantLibGuardar.addActionListener(this);
        vista.jtableLibros.addMouseListener(this);
        vista.jbActualizar.addActionListener(this);
        vista.jButtonEliminar.addActionListener(this);
        vista.jcbCat.setModel(modelo.llenaComboModel());
        vista.jcbCat.addActionListener(this);
        listaLibrosEnTabla();
    }
    


    public void procesoDeGuardado(){
        guardaLibro();
        listaLibrosEnTabla();
        restauraFormulario();
    }
    

    public void procesoDeActualizado(){
        actualizarLibro();
        listaLibrosEnTabla();
        restauraFormulario();
    }
    

    public void procesoDeEliminado(){
        eliminarLibro();
        listaLibrosEnTabla();
        restauraFormulario();
    }
    
    public void eliminarLibro(){

        int confirma = Alerta.confimacion(vista, 
                modelo, 
                Alerta.CONFIRMAR_ELIMINAR
        );
        
        if(confirma == 0){
            seteaValoresModelo();
            if(modelo.delete() > 0)
                Alerta.informacion(vista, 
                        modelo, 
                        Alerta.MENSAJE, 
                        "Datos eliminados correctamente."
                );
            else
                Alerta.informacion(vista, 
                        modelo, 
                        Alerta.MENSAJE_ERROR, 
                        "Ocurrió un error al eliminar."
                );
        }
    }
    
    public void actualizarLibro(){

        int confirma = Alerta.confimacion(vista, 
                modelo, 
                Alerta.CONFIRMAR_ACTUALIZAR
        );
        
        if(confirma == 0){
            seteaValoresModelo();

        if(modelo.put() > 0)
                Alerta.informacion(vista, 
                        modelo, 
                        Alerta.MENSAJE, 
                        "Datos actualizados correctamente."
                );
            else
                Alerta.informacion(vista, 
                        modelo, 
                        Alerta.MENSAJE_ERROR, 
                        "Ocurrió un error al actualizar."
                );
        }
        
    }
    

    public void restauraFormulario(){
        vista.jbActualizar.setEnabled(false);
        vista.jButtonEliminar.setEnabled(false);
        vista.jbMantLibGuardar.setEnabled(true);
        vista.vaciaFormulario();
        vista.jtLibroId.setText("");
        
    }
    

    public void seteaValoresModelo(){
        String nombre = vista.jtMantLibNombre.getText();
        String autor = vista.jtMantPerAutor.getText();
        String editorial = vista.jtMantLibEditorial.getText();
        String ejemplares = vista.jtMantLibEjemplares.getText();
        CatModel modeloTemporal = (CatModel) vista.jcbCat.getSelectedItem();
        String categoria_id = modeloTemporal.getId();
        
              
        modelo.setNombre(nombre);
        modelo.setAutor(autor);
        modelo.setEditorial(editorial);
        modelo.setEjemplares(ejemplares);
        modelo.setCategoria_id(categoria_id);
    }


    public void guardaLibro(){
        seteaValoresModelo();
        
        if(modelo.post() > 0)
            Alerta.informacion(vista, 
                    modelo, 
                    Alerta.MENSAJE, 
                    "Datos ingresados correctamente."
            );
        else
            Alerta.informacion(vista, 
                    modelo, 
                    Alerta.MENSAJE_ERROR, 
                    "Ocurrió un error al ingresar."
            );

    }
    

    public void listaLibrosEnTabla(){
        vista.jtableLibros.setModel(modelo.getAllTableModel());
        
    }
    

    public void pasaDatosAFormulario(){
        vista.jtLibroId.setText(modelo.getId());
        vista.jtMantLibNombre.setText(modelo.getNombre());
        vista.jtMantPerAutor.setText(modelo.getAutor());
        vista.jtMantLibEditorial.setText(modelo.getEditorial());
        vista.jtMantLibEjemplares.setText(modelo.getEjemplares());

    }
    

    public String controlaNulos(Object str){
        if (str == null) str = "null";

        return str.toString();
    }
    

    public void prepararBotones(){
        vista.jbMantLibGuardar.setEnabled(false);
        vista.jbActualizar.setEnabled(true);
        vista.jButtonEliminar.setEnabled(true);
    }
    
    public void cargaDatosTablaAModelo(MouseEvent ex){
        JTable table = (JTable) ex.getSource();
             
        Object id = table.getValueAt(table.getSelectedRow(), 0);
        Object nombre = table.getValueAt( table.getSelectedRow(), 1);
        Object autor = table.getValueAt(table.getSelectedRow(), 2);
        Object editorial = table.getValueAt(table.getSelectedRow(), 3);
        Object ejemplares = table.getValueAt(table.getSelectedRow(), 4);
        Object categoria_id = table.getValueAt(table.getSelectedRow(), 5);
        

        modelo.setId(controlaNulos(id));
        modelo.setNombre(controlaNulos(nombre));
        modelo.setAutor(controlaNulos(autor));
        modelo.setEditorial(controlaNulos(editorial));
        modelo.setEjemplares(controlaNulos(ejemplares));
        modelo.setCategoria_id(controlaNulos(categoria_id));

    }

    
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == vista.jbMantLibGuardar) procesoDeGuardado();
        if(e.getSource() == vista.jbActualizar) procesoDeActualizado();
        if(e.getSource() == vista.jButtonEliminar) procesoDeEliminado();
        
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if(e.getSource() == vista.jtableLibros){
             cargaDatosTablaAModelo(e);
             pasaDatosAFormulario();
             prepararBotones();

        }

    }
    
}
