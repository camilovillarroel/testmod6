/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import javax.swing.JTable;
import models.CatModel;
import views.CatView;
import views.utils.Alerta;

/**
 *
 * @author camilo
 */
public class CatController extends Controller{
    CatView vista;
    CatModel modelo;

    public CatController(CatView vista, CatModel modelo) {
        this.vista = vista;
        this.modelo = modelo;
    }
public void iniciar(){
vista.jbMantCatGuardar.addActionListener(this);
vista.jtableCat.addMouseListener(this);
vista.jbActualizar.addActionListener(this);
vista.jButtonEliminar.addActionListener(this);
listaCatEnTabla();
}        
public void procesoDeGuardado(){
        guardaCat();
        listaCatEnTabla();
        
    }
    

    public void procesoDeActualizado(){
        actualizarCat();
        listaCatEnTabla();
        restauraFormulario();
    }
    

    public void procesoDeEliminado(){
        eliminarCat();
        listaCatEnTabla();
        restauraFormulario();
    }
    public void eliminarCat(){

        int confirma = Alerta.confimacion(vista, 
                modelo, 
                Alerta.CONFIRMAR_ELIMINAR
        );
        
        if(confirma == 0){
            seteaValoresModelo();
            if(modelo.delete() > 0)
                Alerta.informacion(vista, 
                        modelo, 
                        Alerta.MENSAJE, 
                        "Datos eliminados correctamente."
                );
            else
                Alerta.informacion(vista, 
                        modelo, 
                        Alerta.MENSAJE_ERROR, 
                        "Ocurrió un error al eliminar."
                );
        }
    }
    
    public void actualizarCat(){

        int confirma = Alerta.confimacion(vista, 
                modelo, 
                Alerta.CONFIRMAR_ACTUALIZAR
        );
        
        if(confirma == 0){
            seteaValoresModelo();

        if(modelo.put() > 0)
                Alerta.informacion(vista, 
                        modelo, 
                        Alerta.MENSAJE, 
                        "Datos actualizados correctamente."
                );
            else
                Alerta.informacion(vista, 
                        modelo, 
                        Alerta.MENSAJE_ERROR, 
                        "Ocurrió un error al actualizar."
                );
        }
        
    }
    

    public void restauraFormulario(){
        vista.jbActualizar.setEnabled(false);
        vista.jButtonEliminar.setEnabled(false);
        vista.jbMantCatGuardar.setEnabled(true);
        vista.vaciaFormulario();
        vista.jtCatId.setText("");
        
    }
    

    public void seteaValoresModelo(){
        String nombre = vista.jtMantCatNombre.getText();
        String descripcion = vista.jtMantCatDesc.getText();
        
        
        modelo.setNombre(nombre);
        modelo.setDescripcion(descripcion);
        
    }


    public void guardaCat(){
        seteaValoresModelo();
        
        if(modelo.post() > 0)
            Alerta.informacion(vista, 
                    modelo, 
                    Alerta.MENSAJE, 
                    "Datos ingresados correctamente."
            );
        else
            Alerta.informacion(vista, 
                    modelo, 
                    Alerta.MENSAJE_ERROR, 
                    "Ocurrió un error al ingresar."
            );

    }
    

    public void listaCatEnTabla(){
        vista.jtableCat.setModel(modelo.getAllTableModel());
        
    }
    

    public void pasaDatosAFormulario(){
        vista.jtCatId.setText(modelo.getId());
        vista.jtMantCatNombre.setText(modelo.getNombre());
        vista.jtMantCatDesc.setText(modelo.getDescripcion());
        

    }
    

    public String controlaNulos(Object str){
        if (str == null) str = "null";

        return str.toString();
    }
    

    public void prepararBotones(){
        vista.jbMantCatGuardar.setEnabled(false);
        vista.jbActualizar.setEnabled(true);
        vista.jButtonEliminar.setEnabled(true);
    }
    
    public void cargaDatosTablaAModelo(MouseEvent ex){
        JTable table = (JTable) ex.getSource();
             
        Object id = table.getValueAt(table.getSelectedRow(), 0);
        Object nombre = table.getValueAt( table.getSelectedRow(), 1);
        Object descripcion = table.getValueAt(table.getSelectedRow(), 2);
       

        modelo.setId(controlaNulos(id));
        modelo.setNombre(controlaNulos(nombre));
        modelo.setDescripcion(controlaNulos(descripcion));
      
    }

    
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == vista.jbMantCatGuardar) procesoDeGuardado();
        if(e.getSource() == vista.jbActualizar) procesoDeActualizado();
        if(e.getSource() == vista.jButtonEliminar) procesoDeEliminado();
        
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if(e.getSource() == vista.jtableCat){
             cargaDatosTablaAModelo(e);
             pasaDatosAFormulario();
             prepararBotones();

        }

    }
    
}

 