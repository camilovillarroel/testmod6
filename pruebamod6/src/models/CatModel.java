/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author camilo
 */
public class CatModel implements Modelo{
    
    private String id;
    private String nombre;
    private String descripcion;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public int post() {
        int retorno = 0;
        
        Connection connection = MysqlConnection.getConnection();
        
        String consulta = "insert into categoria(nombre,descripcion)"
                + " values( ?, ?)";
        
        try {
            
            PreparedStatement ps = connection.prepareStatement(consulta);
           
            ps.setString(1, this.nombre);
            ps.setString(2, this.descripcion);
          
            
            retorno = ps.executeUpdate();
            
        } catch (SQLException ex) {
            //
        }finally{
            closeThisConnection(connection);
        }
        
        return retorno;
    }

    @Override
    public int put() {
        int retorno = 0;
        
        Connection connection = MysqlConnection.getConnection();
        
        String consulta = "update categoria "
                + "set nombre = ?"
                + ", descripcion = ?"               
                + "where id = ?";
        
        
        
        try {
            
            PreparedStatement ps = connection.prepareStatement(consulta);
           
            ps.setString(1, this.nombre);
            ps.setString(2, this.descripcion);
           
            ps.setInt(3, Integer.parseInt(this.id));
            
            retorno = ps.executeUpdate();
            
        } catch (SQLException ex) {
            //
        }finally{
            closeThisConnection(connection);
        }
        
        return retorno;
    }

    @Override
    public int delete() {
        int retorno = 0;
        
        Connection connection = MysqlConnection.getConnection();
        
        String consulta = "delete from categoria "
                + "where id = ?";

        try {
            
            PreparedStatement ps = connection.prepareStatement(consulta);
           
            ps.setInt(1, Integer.parseInt(this.id));
            
            retorno = ps.executeUpdate();
            
        } catch (SQLException ex) {
            //
        }finally{
            closeThisConnection(connection);
        }
        
        return retorno;
    }

    @Override
    public String muestraAtributos() {
        return ""
                + "- Id: " + id + "\n"
                + "- Nombre: " + nombre + "\n"
                + "- Descripción: " + descripcion + "\n"
               ;
    }
    public void closeThisConnection(Connection con){
        try {
            con.close();
        } catch (SQLException e) {
            // todo
        }
    }
    public DefaultTableModel getAllTableModel(){
        Connection conn = MysqlConnection.getConnection();
        String consulta = ""
                + " select    id"
                + "         ,nombre"
                + "         ,descripcion"
                + " from     categoria;";
        
        
        DefaultTableModel modeloTabla = new DefaultTableModel();

        modeloTabla.addColumn("ID");
        modeloTabla.addColumn("NOMBRE");
        modeloTabla.addColumn("DESCRIPCION");
       
        
        int numcolumnas = modeloTabla.getColumnCount();
        
        try {
            PreparedStatement ps = conn.prepareStatement(consulta);
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                Object[] fila = new Object[numcolumnas];
                
                for(int i = 0; i < numcolumnas; i++){
                    fila[i] = rs.getObject(i + 1);
                }
                
                modeloTabla.addRow(fila);
                
            }
        } catch (SQLException ex) {
            System.out.println("Error en cargaTodasEnTabla: " + ex);
                    
        } finally{
            closeThisConnection(conn);
        }
        return modeloTabla;
    }
    
    @Override
    public String toString(){
        return nombre;
    }
    
    
    
}
