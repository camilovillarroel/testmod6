package models;

import com.mysql.jdbc.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MysqlConnection {
    // variables que pueden cambiar
    private static final String PORT = "3306";
    private static final String HOST = "localhost";
    private static final String DATABASE_NAME = "libreria";
    public static final String USER_NAME = "root";
    public static final String USER_PASSWORD = "camilo11";
    
    // esta no cambia
    public static final String STANDARD = "?autoReconnet=true&useSSL=false";
    
    public static final String MYSQL_URL = "jdbc:mysql://"
            + HOST + ":" + PORT + "/" + DATABASE_NAME + STANDARD;
    
    // esta no cambia
    public static final String DRIVER = "com.mysql.jdbc.Driver";
    
    /**
     * Este es el método que crea la conexión.
     * 
     * @return Connection connection
     */
    public static Connection getConnection(){
        Connection connection = null;
        try {
            Class.forName(DRIVER);
            
            connection = (Connection) DriverManager.getConnection(
                MYSQL_URL,
                USER_NAME,
                USER_PASSWORD
            );

        } catch (ClassNotFoundException ex) {
            System.out.println(ex);
            
        }catch (SQLException ex) {
            System.out.println("Error sql" + ex);
        }
        return connection;
    }
}
