package models;

import java.sql.PreparedStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.DefaultComboBoxModel;
import javax.swing.table.DefaultTableModel;


public class LibroModel implements Modelo{
    private String id;        
    private String nombre;
    private String autor;
    private String editorial; 
    private String ejemplares;
    private String categoria_id;
    

    public String getCategoria_id() {
        return categoria_id;
    }

    public void setCategoria_id(String categoria_id) {
        this.categoria_id = categoria_id;
    }
    
    

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }



    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getEditorial() {
        return editorial;
    }

    public void setEditorial(String editorial) {
        this.editorial = editorial;
    }

    public String getEjemplares() {
        return ejemplares;
    }

    public void setEjemplares(String ejemplares) {
        this.ejemplares = ejemplares;
    }
    
    public DefaultComboBoxModel llenaComboModel(){   
        DefaultComboBoxModel<CatModel> comboModel = new DefaultComboBoxModel();
        
        Connection conn = MysqlConnection.getConnection();
        String consulta = ""
                + " select      id"
                + "             ,nombre"
                + "             ,descripcion"
                + " from        categoria;";
       
        try {
            PreparedStatement ps = conn.prepareStatement(consulta);
            ResultSet rs = ps.executeQuery();
            
            
            while(rs.next()){
                CatModel category = new CatModel();
                category.setId(Integer.toString((Integer) rs.getObject("id")));
                category.setNombre((String) rs.getObject("nombre"));
                

                comboModel.addElement(category);
            }
        } catch (SQLException ex) {
            System.out.println("Error en cargaTodasEnTabla: " + ex);
                    
        } finally{
            closeThisConnection(conn);
        }
        
       return comboModel;
       
    }

    
    
    
    
    @Override
    public int post(){
        int retorno = 0;
        
        Connection connection = MysqlConnection.getConnection();
        
        String consulta = "insert into libro(nombre,autor,editorial,ejemplares,categoria_id)"
                + " values( ?, ?, ?, ?, (select id from categoria where id= ? ) )";
        
        try {
            
            PreparedStatement ps = connection.prepareStatement(consulta);
          
            ps.setString(1, this.nombre);
            ps.setString(2, this.autor);
            ps.setString(3, this.editorial);
            ps.setString(4, this.ejemplares);
            ps.setString(5, this.categoria_id);
            
            retorno = ps.executeUpdate();
            
        } catch (SQLException ex) {
            //
        }finally{
            closeThisConnection(connection);
        }
        
        return retorno;
    }
    
    @Override
    public int put(){
        int retorno = 0;
        
        Connection connection = MysqlConnection.getConnection();
        
        String consulta = "update libro "
                + "set nombre = ?"
                + ", autor = ?"
                + ", editorial = ?"
                + ", ejemplares = ?"
                +", categoria_id= ?"
                + "where id = ?";
        
        
        
        try {
            
            PreparedStatement ps = connection.prepareStatement(consulta);
           
            ps.setString(1, this.nombre);
            ps.setString(2, this.autor);
            ps.setString(3, this.editorial);
            ps.setString(4, this.ejemplares);
            ps.setString(5, this.categoria_id);
            ps.setInt(6, Integer.parseInt(this.id));
            
            retorno = ps.executeUpdate();
            
        } catch (SQLException ex) {
            //
        }finally{
            closeThisConnection(connection);
        }
        
        return retorno;
    }
    
    @Override
    public int delete(){
       int retorno = 0;
        
        Connection connection = MysqlConnection.getConnection();
        
        String consulta = "delete from libro "
                + "where id = ?";

        try {
            
            PreparedStatement ps = connection.prepareStatement(consulta);
           
            ps.setInt(1, Integer.parseInt(this.id));
            
            retorno = ps.executeUpdate();
            
        } catch (SQLException ex) {
            //
        }finally{
            closeThisConnection(connection);
        }
        
        return retorno;
    }
    
    public void closeThisConnection(Connection con){
        try {
            con.close();
        } catch (SQLException e) {
            // todo
        }
    }
    
    
    public DefaultTableModel getAllTableModel(){
        Connection conn = MysqlConnection.getConnection();
        String consulta = ""
            + "select    lb.id "
            + "         ,lb.nombre "
            + "         ,lb.autor "
            + "         ,lb.editorial "
            + "         ,lb.ejemplares "
            + "         ,cat.nombre "
            + "from     libro lb "
            + "join categoria cat "
            + "on lb.categoria_id = cat.id;";

        
        DefaultTableModel modeloTabla = new DefaultTableModel();

        modeloTabla.addColumn("ID");
        modeloTabla.addColumn("NOMBRE");
        modeloTabla.addColumn("AUTOR");
        modeloTabla.addColumn("EDITORIAL");
        modeloTabla.addColumn("EJEMPLARES");
        modeloTabla.addColumn("CATEGORIA");
        
        int numcolumnas = modeloTabla.getColumnCount();
        
        try {
            PreparedStatement ps = conn.prepareStatement(consulta);
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                Object[] fila = new Object[numcolumnas];
                
                for(int i = 0; i < numcolumnas; i++){
                    fila[i] = rs.getObject(i + 1);
                }
                
                modeloTabla.addRow(fila);
                
            }
        } catch (SQLException ex) {
            System.out.println("Error en cargaTodasEnTabla: " + ex);
                    
        } finally{
            closeThisConnection(conn);
        }
        return modeloTabla;
    }
    
    @Override
    public String muestraAtributos(){
        return ""
                + "- Id: " + id + "\n"
                + "- Nombre: " + nombre + "\n"
                + "- Autor: " + autor + "\n"
                + "- Editorial: " + editorial + "\n"
                + "- Ejemplares: " + ejemplares + "\n"
                + "- Categoria: " + categoria_id
                ;
    }
    
}
